module(...,package.seeall)

require"misc"
require"utils"
require"pins"

local ft6336addr = 0x38
local i2cid = 2
if i2c.setup(i2cid,i2c.SLOW) ~= i2c.SLOW then
    print("i2c.init fail")
    return
end

local rst = pins.setup(16, 1)
--local init = pins.setup(17)

--写1字节
local function writeReg(reg,data)
    i2c.send(i2cid,ft6336addr,string.char(reg,data))
end

--读1字节，返回string
local function readReg(reg,len)
    i2c.send(i2cid,ft6336addr,string.char(reg))
    return i2c.recv(i2cid,ft6336addr,len)
end

local function FT6336_Scan()
    --读取触摸点的状态
    local pressed = bit.band(readReg(0x02,1):byte(),0x0f)
    --log.info("ft6336.keyDown",pressed)

    local status = {}
    for i=0,pressed-1 do
        local xh = bit.band(readReg(0x03+6*i,1):byte(),0x0f)
        local xl = readReg(0x04+6*i,1):byte() + xh * 256
        local yh = bit.band(readReg(0x05+6*i,1):byte(),0x0f)
        local yl = readReg(0x06+6*i,1):byte() + yh * 256
        table.insert(status,{x=xl,y=yl})
        --log.info("ft6336.points",i,xl,yl)
    end
    return status
end

--触屏状态存这里好了
pressed = {}

sys.taskInit(function ()
    -------------------------初始化-------------------------
    rst(0)
    sys.wait(50)
    rst(1)
    sys.wait(100)
    writeReg(0x00,0)    --进入正常操作模式
    writeReg(0x80,22)   --设置触摸有效值，22，越小越灵敏
    writeReg(0x88,14)   --激活周期，不能小于12，最大14
    --------------------------------------------------------
    while true do
        pcall(function ()
            pressed = FT6336_Scan()
        end)
        sys.wait(10)
    end
end)


