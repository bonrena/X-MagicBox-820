--必须在这个位置定义PROJECT和VERSION变量
--PROJECT：ascii string类型，可以随便定义，只要不使用,就行
--VERSION：ascii string类型，如果使用Luat物联云平台固件升级的功能，必须按照"X.X.X"定义，X表示1位数字；否则可随便定义
PROJECT = "ADC"
VERSION = "1.0.0"

--加载日志功能模块，并且设置日志输出等级
--如果关闭调用log模块接口输出的日志，等级设置为log.LOG_SILENT即可
require "log"
LOG_LEVEL = log.LOGLEVEL_TRACE
--[[
如果使用UART输出日志，打开这行注释的代码"--log.openTrace(true,1,115200)"即可，根据自己的需求修改此接口的参数
如果要彻底关闭脚本中的输出日志（包括调用log模块接口和Lua标准print接口输出的日志），执行log.openTrace(false,第二个参数跟调用openTrace接口打开日志的第二个参数相同)，例如：
1、没有调用过sys.opntrace配置日志输出端口或者最后一次是调用log.openTrace(true,nil,921600)配置日志输出端口，此时要关闭输出日志，直接调用log.openTrace(false)即可
2、最后一次是调用log.openTrace(true,1,115200)配置日志输出端口，此时要关闭输出日志，直接调用log.openTrace(false,1)即可
]]
--log.openTrace(true,1,115200)

require "sys"

require "net"
--每1分钟查询一次GSM信号强度
--每1分钟查询一次基站信息
net.startQueryAll(60000, 60000)

--此处关闭RNDIS网卡功能
--否则，模块通过USB连接电脑后，会在电脑的网络适配器中枚举一个RNDIS网卡，电脑默认使用此网卡上网，导致模块使用的sim卡流量流失
--如果项目中需要打开此功能，把ril.request("AT+RNDISCALL=0,1")修改为ril.request("AT+RNDISCALL=1,1")即可
--注意：core固件：V0030以及之后的版本、V3028以及之后的版本，才以稳定地支持此功能
ril.request("AT+RNDISCALL=0,1")

--加载控制台调试功能模块（此处代码配置的是uart1，波特率115200）
--此功能模块不是必须的，根据项目需求决定是否加载
--使用时注意：控制台使用的uart不要和其他功能使用的uart冲突
--使用说明参考demo/console下的《console功能使用说明.docx》
--require "console"
--console.setup(1, 115200)

--加载硬件看门狗功能模块
--根据自己的硬件配置决定：1、是否加载此功能模块；2、配置Luat模块复位单片机引脚和互相喂狗引脚
--合宙官方出售的Air201开发板上有硬件看门狗，所以使用官方Air201开发板时，必须加载此功能模块
--[[
require "wdt"
wdt.setup(pio.P0_30, pio.P0_31)
]]

--加载错误日志管理功能模块【强烈建议打开此功能】
--如下2行代码，只是简单的演示如何使用errDump功能，详情参考errDump的api
require "errDump"
errDump.request("udp://ota.airm2m.com:9072")

--加载远程升级功能模块【强烈建议打开此功能】
--如下3行代码，只是简单的演示如何使用update功能，详情参考update的api以及demo/update
--PRODUCT_KEY = "v32xEAKsGTIEQxtqgwCldp5aPlcnPs3K"
--require "update"
--update.request()

--[[
注意，如果使用的是820开发板：
需要短接下面的引脚
G-CLK ---- SPI-SCLK
G-RES ---- SPI-MISO
G-CS  ---- SPI-CSEL
G-DI  ---- SPI-MOSI
G-DC  ---- UART1-CTS

侧面的跳线需要 *按屏幕型号进行选择*
（就是绿色端子边上的3pin排针）
RES ---- 3R
或者
RES ---- 0.47R
]]

pmd.ldoset(15,pmd.LDO_VLCD)

--加载墨水屏功能测试模块
--按需选择
--local eink = require"epd1in54_GDEH0154D27"--老款1.54寸黑白，跳线帽选3R
local eink = require"epd1in54_GDEH0154D67"--新款1.54寸黑白，跳线帽选3R
--local eink = require"epd2in7_GDEW027W3"--2.7寸黑白，跳线帽选0.47R
--local eink = require"epd4in2_GDEW042T2"--4.2寸黑白，跳线帽选0.47R

--加载一下图片数据，注意对应屏幕分辨率
local picData = require("pic200x200")
--local picData = require("pic400x300")

--触屏驱动，基于佳显2.7寸电子纸触摸屏（FT6336G）
--其他屏可以参考适配
--require"ft6336"

--显示点东西
sys.taskInit(function ()
    sys.wait(5000)
    log.info("eink.init")
    eink.init()
    log.info("eink.refresh")
    --清屏
    eink.clear(0xff)

    sys.wait(1000)

    eink.init()
    --刷图
    eink.showPictureN(picData)

    sys.wait(5000)

    --还能利用disp库来处理显示数据
    disp.new({
        width = eink.EPD_WIDTH, --分辨率宽度，128像素；用户根据屏的参数自行修改
        height = eink.EPD_HEIGHT, --分辨率高度，64像素；用户根据屏的参数自行修改
        bpp = 1, --位深度，1表示单色。单色屏就设置为1，不可修改
        yoffset = 0, --Y轴偏移
        xoffset = 0, --X轴偏移
        hwfillcolor = 0xffff, --填充色
    })
    disp.setbkcolor(0xffff)
    disp.clear()
    disp.setcolor(0x0000)
    require"common"
    disp.puttext("Hello World!",10,32)
    disp.puttext(common.utf8ToGb2312("你好世界"),10,64)
    local scr = disp.getframe()

    eink.init()
    eink.showPicturePage(scr)

    --刷触屏效果
    --墨水屏这刷太慢了
    -- local last = ft6336.pressed
    -- while true do
    --     local changed = #last ~= #ft6336.pressed
    --     for i=1,#ft6336.pressed do
    --         if not last[i] or
    --         ft6336.pressed[i].x ~= last[i].x or
    --         ft6336.pressed[i].y ~= last[i].y then
    --             changed = true
    --             break
    --         end
    --     end
    --     if changed then
    --         last = ft6336.pressed
    --         disp.clear()
    --         for i=1,#last do
    --             local x1,x2,y1,y2 = eink.EPD_WIDTH - last[i].y - 5,eink.EPD_WIDTH - last[i].y + 5,last[i].x - 5,last[i].x + 5
    --             if x1 < 0 then x1 = 0 end
    --             if x2 > eink.EPD_WIDTH then x2 = eink.EPD_WIDTH end
    --             if y1 < 0 then y1 = 0 end
    --             if y2 > eink.EPD_HEIGHT then y2 = eink.EPD_HEIGHT end
    --             disp.drawrect(0,y1,eink.EPD_WIDTH-1,y2,0)
    --             disp.drawrect(x1,0,x2,eink.EPD_HEIGHT-1,0)
    --         end
    --         local scr = disp.getframe()
    --         eink.showPicturePage(scr)
    --         sys.wait(3000)
    --     else
    --         sys.wait(300)
    --     end
    -- end

    eink.deepSleep()
end)


--启动系统框架
sys.init(0, 0)
sys.run()
