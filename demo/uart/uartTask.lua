--- 模块功能：串口功能
require "utils"
require "pm"
module(..., package.seeall)

--电压阈开到最大
pmd.ldoset(15,pmd.LDO_VLCD)

-- 串口ID,串口读缓冲区
local UART_ID, sendQueue = 1, {}
-- 串口超时，串口准备好后发布的消息
local uartimeout, recvReady = 500, "UART_RECV_ID"
--保持系统处于唤醒状态，不会休眠
pm.wake("mcuart")
uart.setup(UART_ID, 115200, 8, uart.PAR_NONE, uart.STOP_1)
--需要485通讯时打开下面的语句
--uart.set_rs485_oe(UART_ID, pio.P0_19)
--如果需要打开“串口发送数据完成后，通过异步消息通知”的功能，则使用下面的这行setup，注释掉上面的一行setup
--uart.setup(UART_ID,115200,8,uart.PAR_NONE,uart.STOP_1,nil,1)
uart.on(UART_ID, "receive", function(uid, length)
    table.insert(sendQueue, uart.read(uid, length))
    sys.timerStart(sys.publish, uartimeout, recvReady)
end)

local function writeOk()
    log.info("uartTask.writeOk")
end
--注册串口的数据发送通知函数
uart.on(UART_ID,"sent",writeOk)

--发串口数据
function write(s)
    log.info("uartTask.write",s)
    uart.write(UART_ID,s)
end

-- 向串口发送收到的字符串
sys.subscribe(recvReady, function()
    --拼接所有收到的数据
    local str = table.concat(sendQueue)
    -- 串口的数据读完后清空缓冲区
    sendQueue = {}
    --不用完整打印，会影响运行速度
    log.info("uartTask.read length", #str, str:sub(1,100),str:sub(-100,-1))

    --在这里处理接收到的数据，这是例子
    if str:find(string.char(0x01,0xf1,0xff,0x00)) == 1 then --如果满足开头
        write(string.char(0x0a,0x05)) --回复
    end

end)
