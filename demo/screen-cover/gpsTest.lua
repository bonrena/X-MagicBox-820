local gps = require"gpsZkw"
--agps功能模块只能配合Air800或者Air530使用；如果不是这两款模块，不要打开agps功能
require"agpsZkw"

local function printGps()
    if gps.isOpen() then
        local tLocation = gps.getLocation()
        local speed = gps.getSpeed()
        log.info("GPS",gps.isFix(),
            tLocation.lngType,tLocation.lng,tLocation.latType,tLocation.lat,
            gps.getAltitude(),
            speed,
            gps.getCourse(),
            gps.getViewedSateCnt(),
            gps.getUsedSateCnt())
    end
end

gps.setNmeaMode(2,function(msg) if msg:find("$..GGA") == 1 then log.info("NMEA",msg) end end)
gps.open(gps.DEFAULT,{tag="TEST1",cb=test1Cb})
sys.timerLoopStart(printGps,2000)
