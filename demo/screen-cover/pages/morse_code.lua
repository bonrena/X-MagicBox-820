module(...,package.seeall)

--当前的开关状态
local enable

--音频状态
local beep

local connected

--待发送数据
local temp = {}

--收到的数据
local recv = ""
local playing

function update()
    disp.clear()
    disp.setfontheight(32)
    lcd.CHAR_WIDTH = 16
    lcd.putStringCenter("歪比歪比",120,lcd.gety(0),255,255,255)

    lcd.putStringCenter("外比吧波",120,lcd.gety(3),255,150,255)

    disp.setfontheight(16)
    lcd.CHAR_WIDTH = 8

    lcd.putStringCenter("哔哔哔……哔哔……",120,lcd.gety(3),100,100,100)

    lcd.putStringCenter(table.concat(temp),120,lcd.gety(13),50,255,100)
    lcd.putStringCenter(recv,120,lcd.gety(12),255,255,100)
end

local keyTimer
local long
local sendTimer
local keyEvents = {
    ["3"] = function (e)
        beep = e
        log.info("key",e)
        if e then
            long = nil
            keyTimer = sys.timerStart(function() long = true end, 200)
        else
            table.insert(temp,long and "_" or ".")
            log.info("long?",temp[#temp])
            if keyTimer then sys.timerStop(keyTimer) end
            if sendTimer then sys.timerStop(sendTimer) end
            sendTimer = sys.timerStart(function()
                sys.publish("MORSE_SEND_DATA",table.concat(temp))
                temp = {}
            end,1000)
        end
    end,
}


keyEvents["A"] = keyEvents["3"]
keyEvents["OK"] = keyEvents["3"]

function key(k,e)
    if playing then return end
    if keyEvents[k] then
        keyEvents[k](e)
        page.update()
    end
end

function open()
    playing = nil
    enable = true
    sys.publish("MORSE_SEND_DATA_IND")
    audio.stop()
end

function close()
    enable = nil
    beep = nil
    audio.stop()
    sys.publish("MORSE_SEND_DATA_IND")
end

local beeps = string.rep(string.rep(string.char(0xff,0x7f),4)..string.rep(string.char(0x00,0x80),4),6)
sys.taskInit(function()
    while true do
        if beep then
            audiocore.streamplay(audiocore.PCM,beeps)
        end
        sys.wait(1)
    end
end)


local subscribeTopics = {
    ["morseMessage/+"] = 0,--qos为0
}
--待发送数据缓冲区
local toSend = {}
sys.subscribe("MORSE_SEND_DATA",function (data)
    table.insert(toSend,{
        topic = "morseMessage/"..(misc.getImei() or ""),
        payload = data
    })
    sys.publish("MORSE_SEND_DATA_IND")
end)
sys.taskInit(function()
    local r, data
    while true do
        while not socket.isReady() do sys.wait(1000) end
        local mqttc = mqtt.client(misc.getImei().."MORSE", 300, "user", "password")--心跳时间300秒，用户名密码为默认
        while not enable do sys.wait(1000) end--没开启mqtt
        while not mqttc:connect("lbsmqtt.airm2m.com", 1884) do sys.wait(2000) end
        if mqttc:subscribe(subscribeTopics) then--订阅主题
            connected = true
            while true do
                r, data = mqttc:receive(120000, "MORSE_SEND_DATA_IND")
                if r then
                    log.info("mqtt receive", data.topic,data.payload)
                    sys.publish("MORSE_RECEIVE",data)
                elseif data == "MORSE_SEND_DATA_IND" then
                    if not enable then break end--强制断开mqtt连接
                    local ar
                    while #toSend > 0 do
                        local mdata = table.remove(toSend,1)
                        log.info("mqtt send",mdata.topic,mdata.payload)
                        local mr = mqttc:publish(mdata.topic, mdata.payload)
                        if not mr then ar = true break end
                    end
                    if ar then break end
                elseif data == "timeout" then
                    --没必要处理这东西
                else
                    break
                end
            end
        end
        connected = nil
        r, data = nil,nil
        mqttc:disconnect()
    end
end)

-- 收到mqtt数据的处理
sys.subscribe("MORSE_RECEIVE",function (params)
    if playing then return end
    if params.topic ~= "morseMessage/"..misc.getImei() then
        playing = true
        sys.taskInit(function()
            for i=1,#params.payload do
                recv = params.payload:sub(1,i)
                page.update()
                if not enable then break end
                if params.payload:sub(i,i) == "." then
                    beep = true
                    sys.wait(50)
                    beep = false
                else
                    beep = true
                    sys.wait(300)
                    beep = false
                end
                sys.wait(200)
            end
            playing = nil
        end)
    end
end)
