module(...,package.seeall)
require"tools"
require"audio"
-- The CHIP-8 emulator.
local CHIP8 = require("chip8")
-- The CHIP-8 emulator object.
local cpu
-- RGB value for being fully opaque.
local OPAQUE = lcd.rgb(51, 255, 102)
-- The dimensions of the screen.
local DISPLAY_W = 64
local DISPLAY_H = 32
-- Side length for one pixel.
local PIXEL_SIZE = 3

-- local KEY_MAPPING = {
--     ["1"] = 0x1, --1
--     ["2"] = 0x2, --2
--     ["3"] = 0x3, --3
--     ["4"] = 0xC, --4
--     ["Q"] = 0x4, --Q
--     ["W"] = 0x5, --W
--     ["E"] = 0x6, --E
--     ["R"] = 0xD, --R
--     ["A"] = 0x7, --A
--     ["S"] = 0x8, --S
--     ["D"] = 0x9, --D
--     ["f"] = 0xE, --F
--     ["z"] = 0xA, --Z
--     ["x"] = 0x0, --X
--     ["c"] = 0xB, --C
--     ["v"] = 0xF, --V
-- }

local gameNow = 1
local gameList = {
    {
        name = "slipperyslope",
        des = "冰上行走，摇杆控制",
        speed = 20,
        color = lcd.rgb(0, 0, 128),
        bk = lcd.rgb(176, 224, 230),
        KEY_MAPPING = {
            ["UP"] = 0x5, --W
            ["LEFT"] = 0x7, --A
            ["DOWN"] = 0x8, --S
            ["RIGHT"] = 0x9, --D
        }
    },
    {
        name = "SuperAstroDodge",
        des = "躲避陨石，摇杆控制，按A开始",
        speed = 50,
        color = lcd.rgb(180,200,180),
        bk = lcd.rgb(80,80,80),
        hires = true,
        flip = true,
        KEY_MAPPING = {
            ["UP"] = 0x2, --2
            ["DOWN"] = 0x8, --S
            ["LEFT"] = 0x4, --Q
            ["RIGHT"] = 0x6, --E
            ["A"] = 0x5, --W
        }
    },
    {
        name = "SuperWorm",
        des = "贪吃蛇，摇杆控制",
        speed = 50,
        color = lcd.rgb(200,150,150),
        bk = lcd.rgb(20,20,20),
        hires = true,
        KEY_MAPPING = {
            ["UP"] = 0x2, --2
            ["DOWN"] = 0x8, --S
            ["LEFT"] = 0x4, --Q
            ["RIGHT"] = 0x6, --E
            ["A"] = 0x5, --W
        }
    },
    {
        name = "UFO",
        des = "打UFO，按上、左、右发射",
        speed = 10,
        bk = lcd.rgb(50, 50, 50),
        KEY_MAPPING = {
            ["LEFT"] = 0x4, --Q
            ["UP"] = 0x5, --W
            ["RIGHT"] = 0x6, --E
        }
    },
    {
        name = "BRIX",
        des = "敲砖块，摇杆控制左右",
        speed = 10,
        color = lcd.rgb(200,200,180),
        bk = lcd.rgb(20,20,20),
        KEY_MAPPING = {
            ["LEFT"] = 0x4, --Q
            ["RIGHT"] = 0x6, --E
        }
    },
    {
        name = "GUESS",
        des = "猜数，看见按A，没看见按B",
        speed = 50,
        color = lcd.rgb(200,200,180),
        bk = lcd.rgb(20,20,20),
        KEY_MAPPING = {
            ["B"] = 0x4, --Q
            ["A"] = 0x5, --W
        }
    },
    {
        name = "HIDDEN",
        des = "找成对的卡片，按A翻面",
        speed = 50,
        color = lcd.rgb(230,255,230),
        bk = lcd.rgb(40,60,40),
        KEY_MAPPING = {
            ["UP"] = 0x2, --2
            ["DOWN"] = 0x8, --S
            ["LEFT"] = 0x4, --Q
            ["RIGHT"] = 0x6, --E
            ["A"] = 0x5, --W
        }
    },
    {
        name = "TANK",
        des = "坦克，摇杆控制，按A射击",
        speed = 30,
        bk = lcd.rgb(20,20,20),
        KEY_MAPPING = {
            ["DOWN"] = 0x2, --2
            ["UP"] = 0x8, --S
            ["LEFT"] = 0x4, --Q
            ["RIGHT"] = 0x6, --E
            ["A"] = 0x5, --W
        }
    },
    {
        name = "TETRIS",
        des = "俄罗斯方块，A旋转",
        speed = 20,
        color = lcd.rgb(255,255,100),
        bk = lcd.rgb(20,20,20),
        KEY_MAPPING = {
            ["A"] = 0x4, --Q
            ["RIGHT"] = 0x6, --E
            ["LEFT"] = 0x5, --W
        }
    },
    {
        name = "WIPEOFF",
        des = "也是敲砖块，摇杆控制",
        speed = 10,
        color = lcd.rgb(255,100,255),
        bk = lcd.rgb(20,20,20),
        KEY_MAPPING = {
            ["LEFT"] = 0x4, --Q
            ["RIGHT"] = 0x6, --E
        }
    },
    {
        name = "MAZE",
        des = "生成随机迷宫，不可互动",
        speed = 50,
        color = lcd.rgb(255,100,100),
        bk = lcd.rgb(20,20,20),
        KEY_MAPPING = {
        }
    },
    {
        name = "BLINKY",
        des = "吃豆人，摇杆控制方向",
        speed = 50,
        color = lcd.rgb(80,100,80),
        KEY_MAPPING = {
            ["UP"] = 0x3, --3
            ["DOWN"] = 0x6, --E
            ["LEFT"] = 0x7, --A
            ["RIGHT"] = 0x8, --S
        }
    },
    {
        name = "MISSILE",
        des = "按A射击",
        speed = 10,
        bk = lcd.rgb(20,20,20),
        KEY_MAPPING = {
            ["A"] = 0x8, --S
        }
    },
    {
        name = "snake",
        des = "贪吃蛇，摇杆控制",
        speed = 10,
        bk = lcd.rgb(20,20,20),
        KEY_MAPPING = {
            ["UP"] = 0x5, --W
            ["LEFT"] = 0x7, --A
            ["DOWN"] = 0x8, --S
            ["RIGHT"] = 0x9, --D
        }
    },
}

local function loadGame()
    cpu = nil
    collectgarbage("collect")
    log.info("chip8","load",gameList[gameNow].name)
    local file,e = io.open("/lua/"..gameList[gameNow].name,"rb")
    if file then
        -- The game instructions.
        local game = {}

        local temp
        -- Load each byte from the ROM into game.
        while true do
            temp = file:read(1)
            if not temp then break end
            game[#game + 1] = temp:byte()

            if (not game[#game]) then
                log.info("chip8","Failed to read ROM! ("..(game[#game] or "nil")..")")
                return
            end
        end
        file:close()

        -- Start the game.
        cpu = CHIP8()
        cpu:setConfig(gameList[gameNow].speed,64,gameList[gameNow].hires and 64 or 32)
        cpu:reset()
        cpu:load(game)
        collectgarbage("collect")

    else
        log.info("chip8",e)
    end
end

local timer_id

function update()
    if not cpu then return end
    local result = cpu:cycle()--跑一个时钟周期
    if result then
        log.info("chip8","ERROR: ",result)
    end

    if cpu.ST > 0 then
        log.info("chip8","BEEP!")
    end

    disp.clear()
    if not gameList[gameNow].hires then
        disp.setfontheight(32)
        lcd.CHAR_WIDTH = 16
        lcd.putStringCenter("CHIP8模拟器",120,0,255,255,255)
        disp.setfontheight(16)
        lcd.CHAR_WIDTH = 8
        lcd.putStringCenter("当前游戏："..gameList[gameNow].name,120,lcd.gety(2),255,255,255)
        lcd.putStringCenter("按4、5键切换游戏",120,lcd.gety(3),50,50,255)

        if gameList[gameNow].bk then
            disp.drawrect(24,72,215,167,gameList[gameNow].bk)
        end
        for y = 0, cpu.height - 1 do
            for x = 0, cpu.width - 1 do
                if cpu.display[x + (y * cpu.width)] > 0 then
                    disp.drawrect(
                        24+ x*PIXEL_SIZE,
                        72+y*PIXEL_SIZE,
                        24+ x*PIXEL_SIZE+2,
                        72+y*PIXEL_SIZE+2,
                        gameList[gameNow].color or OPAQUE)
                end
            end
        end
    else
        if gameList[gameNow].bk then
            disp.drawrect(24,24,215,215,gameList[gameNow].bk)
        end
        for y = 0, cpu.height - 1 do
            for x = 0, cpu.width - 1 do
                if cpu.display[x + (y * cpu.width)] > 0 then
                    if gameList[gameNow].flip then
                        x = x + (x >= 32 and -32 or 32)
                    end
                    disp.drawrect(
                        24+ x*PIXEL_SIZE,
                        24+y*PIXEL_SIZE,
                        24+ x*PIXEL_SIZE+2,
                        24+y*PIXEL_SIZE+2,
                        gameList[gameNow].color or OPAQUE)
                end
            end
        end
        lcd.putStringCenter("当前游戏："..gameList[gameNow].name,120,0,255,255,255)
    end
    if gameList[gameNow].des then
        lcd.putStringCenter(gameList[gameNow].des,120,lcd.gety(14),50,255,100)
    end
end

local keyEvents = {
    ["4"] = function ()
        gameNow = tools.loopAdd(gameNow-1,#gameList,1)
        loadGame()

    end,
    ["5"] = function ()
        gameNow = tools.loopAdd(gameNow+1,#gameList,1)
        loadGame()
    end,
}

function key(k,e)
    if keyEvents[k] and e then
        keyEvents[k]()
        page.update()
    end
    if gameList[gameNow].KEY_MAPPING[k] then
        cpu:setKeyDown(gameList[gameNow].KEY_MAPPING[k], e)
    end
end

function open()
    loadGame()
    timer_id = sys.timerLoopStart(page.update,1)
end

function close()
    if timer_id then
        sys.timerStop(timer_id)
        timer_id = nil
    end
    cpu = nil
    collectgarbage("collect")
end
