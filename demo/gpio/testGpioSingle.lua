--- 模块功能：GPIO功能测试.
-- @author openLuat
-- @module gpio.testGpioSingle
-- @license MIT
-- @copyright openLuat
-- @release 2018.03.27

require"pins"
module(...,package.seeall)

--[[
有些GPIO需要打开对应的ldo电压域才能正常工作，电压域和对应的GPIO关系如下
pmd.ldoset(x,pmd.LDO_VSIM1) -- GPIO 29、30、31
pmd.ldoset(x,pmd.LDO_VLCD) -- GPIO 0、1、2、3、4
pmd.ldoset(x,pmd.LDO_VMMC) -- GPIO 24、25、26、27、28
x=0时：关闭LDO
x=1时：LDO输出1.716V
x=2时：LDO输出1.828V
x=3时：LDO输出1.939V
x=4时：LDO输出2.051V
x=5时：LDO输出2.162V
x=6时：LDO输出2.271V
x=7时：LDO输出2.375V
x=8时：LDO输出2.493V
x=9时：LDO输出2.607V
x=10时：LDO输出2.719V
x=11时：LDO输出2.831V
x=12时：LDO输出2.942V
x=13时：LDO输出3.054V
x=14时：LDO输出3.165V
x=15时：LDO输出3.177V
]]


local level = 0
pmd.ldoset(2,pmd.LDO_VLCD) 
-- GPIO1配置为输出，默认输出低电平，可通过setGpio1Fnc(0或者1)设置输出电平
local setGpio1Fnc = pins.setup(pio.P0_1,0)
sys.timerLoopStart(function()
    level = level==0 and 1 or 0
    setGpio1Fnc(level)
    log.info("GPIO1的状态:",level)
end,1000)

-- GPIO19配置为输入，可通过getGpio19Fnc()获取输入电平
local getGpio19Fnc = pins.setup(pio.P0_19)
sys.timerLoopStart(function()
    log.info("GPIO19的状态",getGpio19Fnc())
end,1000)

-- GPIO18配置为中断，可通过getGpio18Fnc()获取输入电平，产生中断时，自动执行gpio18IntFnc函数
-- 产生中断时自动调用intFnc(msg)函数：上升沿中断时：msg为cpu.INT_GPIO_POSEDGE；下降沿中断时：msg为cpu.INT_GPIO_NEGEDGE
function gpio18IntFnc(msg)
    log.info("GPIO18中断回调",msg,getGpio18Fnc())
    if msg==cpu.INT_GPIO_POSEDGE then
        log.info("GPIO18中断回调", "上升沿中断")
    else
        log.info("GPIO18中断回调", "下降沿中断")
    end
end
getGpio18Fnc = pins.setup(pio.P0_18,gpio18IntFnc)
